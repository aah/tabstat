package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"math"
	"os"
	"sort"
	"strconv"
)

type TabColumn struct {
	Title string
	Col   int
	N     int
	Data  []float64
	Sum   float64
	Rms   float64
	Med   float64
	Min   float64
	Max   float64
	Avg   float64
	Pct5  float64
	Pct10 float64
	Pct25 float64
	Pct50 float64
	Pct75 float64
	Pct85 float64
	Pct90 float64
	Pct95 float64
}

func (t *TabColumn) add(val float64) {
	t.Data = append(t.Data, val)
	t.Max = math.Max(t.Max, val)
	t.Min = math.Min(t.Min, val)
	t.Rms = t.Rms + math.Pow(val, 2.0)
	t.Sum = t.Sum + val
	t.N += 1
}

func (t *TabColumn) finalize() {
	sort.Float64s(t.Data)

	if t.N%2 == 0 {
		x := t.N / 2
		t.Med = (t.Data[x] + t.Data[x-1]) / 2
	} else {
		t.Med = t.Data[t.N/2]
	}

	t.Avg = t.Sum / float64(t.N)
	t.Rms = math.Sqrt(t.Rms / float64(t.N))
	t.Pct5 = t.Data[t.N/20*1]
	t.Pct10 = t.Data[t.N/20*2]
	t.Pct25 = t.Data[t.N/20*5]
	t.Pct50 = t.Data[t.N/20*10]
	t.Pct75 = t.Data[t.N/20*15]
	t.Pct85 = t.Data[t.N/20*17]
	t.Pct90 = t.Data[t.N/20*18]
	t.Pct95 = t.Data[t.N/20*19]
}

func isIntish(val float64) bool {
	return val-math.Floor(val) < 0.00000001
}

func fmtVal(val float64) {
	if isIntish(val) {
		fmt.Printf(" %14d", int(val))
	} else {
		fmt.Printf(" %14.4f", val)
	}
}

func main() {
	columns := make([]TabColumn, 1)

	reader := csv.NewReader(os.Stdin)
	reader.Comma = '\t'
	reader.FieldsPerRecord = -1

	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			fmt.Println("Error: ", err)
			os.Exit(1)
		}

		for index := range record {
			if len(columns) <= index {
				columns = append(columns, TabColumn{
					Min: math.MaxFloat64,
					Max: -math.MaxFloat64,
				})
			}

			val, err := strconv.ParseFloat(record[index], 64)
			if err == nil {
				columns[index].add(val)
			}
		}
	}

	for index := range columns {
		columns[index].finalize()
	}

	fmt.Println("###")
	fmt.Print("sel")
	for index := 0; index < len(columns); index++ {
		fmt.Print("         ------")
	}
	fmt.Println("")

	fmt.Print("n  ")
	for index := range columns {
		fmt.Printf(" %14d", columns[index].N)
	}
	fmt.Println("")

	fmt.Print("ave")
	for index := range columns {
		fmtVal(columns[index].Avg)
	}
	fmt.Println("")

	fmt.Print("rms")
	for index := range columns {
		fmtVal(columns[index].Rms)
	}
	fmt.Println("")

	fmt.Print("med")
	for index := range columns {
		fmtVal(columns[index].Med)
	}
	fmt.Println("")

	fmt.Print("min")
	for index := range columns {
		fmtVal(columns[index].Min)
	}
	fmt.Println("")

	fmt.Print(" %5")
	for index := range columns {
		fmtVal(columns[index].Pct5)
	}
	fmt.Println("")

	fmt.Print("%10")
	for index := range columns {
		fmtVal(columns[index].Pct10)
	}
	fmt.Println("")

	fmt.Print("%25")
	for index := range columns {
		fmtVal(columns[index].Pct25)
	}
	fmt.Println("")

	fmt.Print("%50")
	for index := range columns {
		fmtVal(columns[index].Pct50)
	}
	fmt.Println("")

	fmt.Print("%75")
	for index := range columns {
		fmtVal(columns[index].Pct75)
	}
	fmt.Println("")

	fmt.Print("%85")
	for index := range columns {
		fmtVal(columns[index].Pct85)
	}
	fmt.Println("")

	fmt.Print("%90")
	for index := range columns {
		fmtVal(columns[index].Pct90)
	}
	fmt.Println("")

	fmt.Print("%95")
	for index := range columns {
		fmtVal(columns[index].Pct95)
	}
	fmt.Println("")

	fmt.Print("max")
	for index := range columns {
		fmtVal(columns[index].Max)
	}
	fmt.Println("")

	fmt.Print("sum")
	for index := range columns {
		fmtVal(columns[index].Sum)
	}
	fmt.Println("")

	fmt.Printf("###\n\n")
}
